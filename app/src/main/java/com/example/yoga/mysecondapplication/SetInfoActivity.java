package com.example.yoga.mysecondapplication;

import android.app.LocalActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.File;


public class SetInfoActivity extends ActionBarActivity {

    public static final String BROADCAST = "Image";
    File directory;
    public static final String TAB = "tab";
    public static final String POSITION = "position";
    public static final int TAB_IMAGE = 0;
    public static final int TAB_VIDEO = 1;
    public static final int TAB_NOTE = 2;
    private static final int NEW_PHOTO = 10;
    private static final int NEW_VIDEO = 11;
    private static final int SELECT_PHOTO = 20;
    private static final int SELECT_VIDEO = 21;
    private static final int NEW_NOTE = 12;
    public static final int CM_DELETE_ID = 100;
    public static final int CM_CHANGE_ID = 101;
    public static final String ACTION = "action";
    LocalActivityManager mLocalActivityManager;
    BroadcastReceiver broadcastReceiver;
    final String TAG = "myLogs";
    String path = "";
    String generatedPath;
SaveMarker sMarker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_info);
        sMarker = getIntent().getParcelableExtra("marker");
        createDirectory(); //создаем директорию, куда записываем все файлы

        mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);

        update(TAB_IMAGE);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int tabNumber = intent.getIntExtra(TAB, 0);
                int itemPosition = intent.getIntExtra(POSITION, 0);
                switch (tabNumber){
                    case TAB_IMAGE:{
                        sMarker.delImagePath(itemPosition);
                        update(TAB_IMAGE);
                        break;
                    }
                    case TAB_VIDEO:{
                        sMarker.delVideoPath(itemPosition);
                        update(TAB_VIDEO);
                        break;
                    }
                    case TAB_NOTE:{
                        int action = intent.getIntExtra(ACTION, 0);
                        if (action == CM_DELETE_ID)
                            sMarker.delNote(itemPosition);
                        else if (action == CM_CHANGE_ID) {
                        String note = sMarker.getNote().get(itemPosition);
                        //sMarker.delNote(itemPosition);
                        startActivityForResult(new Intent(SetInfoActivity.this, AddNoteActivity.class).putExtra("note", note).putExtra(POSITION, itemPosition), NEW_NOTE);
                        }
                        update(TAB_NOTE);
                        break;
                    }
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST));
    }

    public void newImage() {
        //создаем намерение, в котором открываем стандартное приложение камеры для того, чтобы сделать фото; формируем путь
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri generatedUri;
        generatedUri = generateFile(NEW_PHOTO);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, generatedUri);
        startActivityForResult(intent, NEW_PHOTO);

    }
    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        TabHost mTabHost = (TabHost) findViewById(R.id.tabHost);
        outState.putInt(TAB, mTabHost.getCurrentTab());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int currentTab = savedInstanceState.getInt(TAB);
        update(currentTab);
    }

    public void newVideo() {
        //создаем намерение, в котором открываем стандартное приложение камеры для того, чтобы сделать видео; формируем путь
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        Uri generatedUri;
        generatedUri = generateFile(NEW_VIDEO);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, generatedUri);
        startActivityForResult(intent, NEW_VIDEO);
    }

    public void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_PHOTO);
    }

    public void selectVideo() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_VIDEO);
    }

    @Override
    //после работы камеры получаем результат
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode){
            case NEW_PHOTO: {
                if (resultCode == RESULT_OK) {
                    sMarker.setImagePath(generatedPath);
                    Toast.makeText(SetInfoActivity.this, "Фото сохранено в " + generatedPath, Toast.LENGTH_LONG).show();
                }
                else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(SetInfoActivity.this,"Фото не сделано" , Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Canceled");
                }
                update(TAB_IMAGE);
                break;
            }
            case NEW_VIDEO: {
                if (resultCode == RESULT_OK) {
                    sMarker.setVideoPath(generatedPath);
                    Toast.makeText(SetInfoActivity.this.getBaseContext(), "Видео сохранено в " + generatedPath, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Video uri: " + path);
                }
                else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(SetInfoActivity.this,"Видео не сделано" , Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Canceled");
                }
                update(TAB_VIDEO);
            break;
            }
            case SELECT_PHOTO: {
                Uri selectedImage = intent.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                sMarker.setImagePath(picturePath);
                Toast toast = Toast.makeText(SetInfoActivity.this, "Фото взято из " + picturePath  , Toast.LENGTH_LONG);
                toast.show();
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                update(TAB_IMAGE);
                break;
            }
            case SELECT_VIDEO: {
                Uri selectedVideo = intent.getData();
                String[] filePath = { MediaStore.Video.Media.DATA };
                Cursor c = getContentResolver().query(selectedVideo,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                sMarker.setVideoPath(videoPath);
                Toast toast = Toast.makeText(SetInfoActivity.this, "Видео взято из " + videoPath  , Toast.LENGTH_LONG);
                toast.show();
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                c.close();
                update(TAB_VIDEO);
                break;
            }
                case NEW_NOTE: {
                    if (resultCode == RESULT_OK) {
                        String note = intent.getStringExtra("note");
                        int position = intent.getIntExtra(POSITION, -1);
                            if (position == -1)
                            sMarker.setNote(note);
                        else {
                                sMarker.delNote(position);
                                sMarker.setNote(note, position);
                        }
                    }
                    else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(SetInfoActivity.this, "Заметка не создана", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Canceled");
                    }
                    update(TAB_NOTE);
                    break;
                }
        }

    }
void update(int tab){
    TabHost mTabHost = (TabHost) findViewById(R.id.tabHost);
    mTabHost.setup(mLocalActivityManager);
    mTabHost.clearAllTabs();
    mLocalActivityManager.removeAllActivities();
    mTabHost.addTab(mTabHost.newTabSpec("image_tab").setIndicator("Images").setContent(new Intent(this, ImageViewGrid.class).putExtra("mImagePath", sMarker.getImagePath())));
    mTabHost.addTab(mTabHost.newTabSpec("video_tab").setIndicator("Videos").setContent(new Intent(this, VideoViewList.class).putExtra("mVideoPath", sMarker.getVideoPath())));
    mTabHost.addTab(mTabHost.newTabSpec("note_tab").setIndicator("Notes").setContent(new Intent(this, TextViewList.class).putExtra("mNote", sMarker.getNote())));
    mTabHost.setCurrentTab(tab);
}

    private Uri generateFile(int type) {
        File file = null;
        switch (type) {
            case NEW_PHOTO:
                file = new File(directory.getPath() + "/" + "App_photo_" + System.currentTimeMillis() + ".jpg");
                break;
            case NEW_VIDEO:
                file = new File(directory.getPath() + "/" + "App_video_" + System.currentTimeMillis() + ".mp4");
                break;
        }
        Log.d(TAG, "fileName = " + file);
        generatedPath = file.getAbsolutePath();
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"AppFiles");
        if (!directory.exists())
            directory.mkdirs();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_set_info, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.image_new_item:
                newImage();
                break;
            case R.id.image_select_item:
                selectImage();
                break;
            case R.id.video_new_item:
                newVideo();
                break;
            case R.id.video_select_item:
                selectVideo();
                break;
            case R.id.note_menu:
                startActivityForResult(new Intent(SetInfoActivity.this, AddNoteActivity.class), NEW_NOTE);
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("marker", sMarker);
        setResult(RESULT_OK, intent);
        unregisterReceiver(broadcastReceiver);
        finish();
    }
}
