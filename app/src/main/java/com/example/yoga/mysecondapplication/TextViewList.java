package com.example.yoga.mysecondapplication;

    import android.app.ListActivity;
    import android.content.Intent;
    import android.os.Bundle;
    import android.view.ContextMenu;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.AdapterView;
    import android.widget.ArrayAdapter;
    import android.widget.ListView;
    import android.widget.Toast;

    import java.util.ArrayList;

public class TextViewList extends ListActivity {

    ArrayList<String> textNotes;
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            textNotes = new ArrayList<String>();
            ArrayList<String> mNote = getIntent().getStringArrayListExtra("mNote");
            if (mNote != null) {
                for (String path : mNote)
                    textNotes.add(path);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, textNotes);
            setListAdapter(adapter);
            ListView listView = getListView();
            registerForContextMenu(listView);
        }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, SetInfoActivity.CM_DELETE_ID, 0, "Delete");
        menu.add(0, SetInfoActivity.CM_CHANGE_ID, 1, "Change");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Intent intent = new Intent(SetInfoActivity.BROADCAST);
        intent.putExtra(SetInfoActivity.TAB, SetInfoActivity.TAB_NOTE);
        if (item.getItemId() == SetInfoActivity.CM_DELETE_ID)
            intent.putExtra(SetInfoActivity.ACTION, SetInfoActivity.CM_DELETE_ID);
        else if (item.getItemId() == SetInfoActivity.CM_CHANGE_ID)
            intent.putExtra(SetInfoActivity.ACTION, SetInfoActivity.CM_CHANGE_ID);
        intent.putExtra(SetInfoActivity.POSITION, adapterContextMenuInfo.position);
        sendBroadcast(intent);
        //String note = (String) getListAdapter().getItem(adapterContextMenuInfo.position);
        return super.onContextItemSelected(item);
    }

        @Override
        protected void onListItemClick(ListView l, View v, int position, long id) {
            String item = (String) getListAdapter().getItem(position);
            Toast.makeText(this, item + " выбран", Toast.LENGTH_LONG).show();
        }

    @Override
    public void onBackPressed() {
        this.getParent().onBackPressed();
    }
    }
