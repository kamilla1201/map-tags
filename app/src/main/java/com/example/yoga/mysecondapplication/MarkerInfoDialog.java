package com.example.yoga.mysecondapplication;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Yoga on 24.02.2015.
 */
public class MarkerInfoDialog extends DialogFragment implements Switch.OnCheckedChangeListener,  View.OnClickListener {

    public MarkerInfoDialog() {
    }

    SaveMarker sMarker;
    boolean isSaved;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_marker_info, null);
        view.findViewById(R.id.more_info_button).setOnClickListener(this);
        TextView tvName = (TextView) view.findViewById(R.id.textView);
        Switch switchbar = (Switch) view.findViewById((R.id.switch_bar));

        isSaved = getArguments().getBoolean("isSaved");
        if (isSaved == true) {
            sMarker = getArguments().getParcelable("isSavedMarker");
            switchbar.setChecked(true);
        } else {
            sMarker = new SaveMarker(0, Double.toString(getArguments().getDouble("latitude")),
                    Double.toString(getArguments().getDouble("longitude")),
                    getArguments().getString("title"));
        }
        getDialog().setTitle("Place info");
        tvName.setText(sMarker.getMOptions().getTitle());
        switchbar.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            isSaved = true;
            Toast.makeText(getActivity(), "Marker is added to the Map", Toast.LENGTH_SHORT).show();
        } else {
            isSaved = false;
            Toast.makeText(getActivity(), "Marker is dropped from the Map", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        MapsActivity callingActivity = (MapsActivity) getActivity();
        callingActivity.onResultDialog(isSaved, sMarker);
        dialog.cancel();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        sMarker = data.getParcelableExtra("marker");
        Toast.makeText(getActivity(), "Информация изменена", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.more_info_button:
                getActivity().startActivityForResult(new Intent(this.getActivity(), SetInfoActivity.class).putExtra("marker", sMarker), 1);
                break;
            default:
                break;
        }
    }
}
