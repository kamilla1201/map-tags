package com.example.yoga.mysecondapplication;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Video.Thumbnails;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class VideoViewList extends ListActivity {

    ArrayList<String> videoFileList = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<String> mVideoPath = getIntent().getStringArrayListExtra("mVideoPath");
        if (mVideoPath != null) {
            for (String path : mVideoPath)
                videoFileList.add(path);
        }
        setListAdapter(new MyThumbnaildapter(VideoViewList.this, R.layout.item_video, videoFileList));
        ListView listView = getListView();
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String videoPath = videoFileList.get(position);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                if (videoPath.contains("file://"))
                    intent.setDataAndType(Uri.parse(videoPath), "video/*");
                else
                    intent.setDataAndType(Uri.parse("file://" + videoPath), "video/*");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, SetInfoActivity.CM_DELETE_ID, 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == SetInfoActivity.CM_DELETE_ID){
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Intent intent = new Intent(SetInfoActivity.BROADCAST);
            intent.putExtra(SetInfoActivity.TAB, SetInfoActivity.TAB_VIDEO);
            intent.putExtra(SetInfoActivity.POSITION, adapterContextMenuInfo.position);
            sendBroadcast(intent);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.getParent().onBackPressed();
    }

    public class MyThumbnaildapter extends ArrayAdapter<String> {

        public MyThumbnaildapter(Context context, int textViewResourceId,
                                 ArrayList<String> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.item_video, parent, false);
            }

            TextView textfilePath = (TextView) row.findViewById(R.id.FilePath);
            textfilePath.setText(new File(videoFileList.get(position)).getName());
            ImageView imageThumbnail = (ImageView) row.findViewById(R.id.Thumbnail);

            Bitmap bmThumbnail;
            //bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoFileList.get(position), Thumbnails.MICRO_KIND);
           //imageThumbnail.setImageBitmap(bmThumbnail);
            imageThumbnail.setTag(videoFileList.get(position));
            new LoadImage(imageThumbnail).execute();

            return row;
        }

    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imv;
        private String path;

        public LoadImage(ImageView imv) {
            this.imv = imv;
            this.path = imv.getTag().toString();
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, Thumbnails.MICRO_KIND);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            //  if (!imv.getTag().toString().equals(path)) {
               /* The path is not same. This means that this
                  image view is handled by some other async task.
                  We don't do anything and return. */
            //      return;
            // }

            if(result != null && imv != null){
                imv.setVisibility(View.VISIBLE);
                imv.setImageBitmap(result);
            }else{
                imv.setVisibility(View.GONE);
            }
        }
    }
}