package com.example.yoga.mysecondapplication;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Created by Yoga on 01.03.2015.
 */
public class ProximityReciever extends BroadcastReceiver {

    public static final String EVENT_ID_INTENT_EXTRA = "EventIDIntentExtraKey";

    @Override
    public void onReceive(Context context, Intent intent) {
        String notificationTitle, notificationMessage;
        int eventID = intent.getIntExtra(EVENT_ID_INTENT_EXTRA, -1);

        boolean proximity_entering = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, true);

        SaveMarker sMarker = intent.getParcelableExtra("marker");

        if(proximity_entering){
            Toast.makeText(context, "Enter the region", Toast.LENGTH_LONG).show();
            notificationTitle = "You're near the saved place!";
            notificationMessage = "Touch to see more : " + eventID;
            Intent notificationIntent = new Intent(context, MapsActivity.class);
            notificationIntent.setData(Uri.parse("tel:/"+eventID));
            notificationIntent.putExtra("marker"+eventID, sMarker);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setWhen(System.currentTimeMillis())
                    .setContentText(notificationMessage)
                    .setContentTitle(notificationTitle)
                    .setSmallIcon(R.drawable.icon)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            Notification notification = notificationBuilder.build();
            nManager.notify(eventID, notification);

        }else{
            Toast.makeText(context,"Exiting the region"  ,Toast.LENGTH_LONG).show();
        }
    }
}