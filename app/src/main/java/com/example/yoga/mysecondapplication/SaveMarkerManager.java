package com.example.yoga.mysecondapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

class SaveMarker implements Parcelable {
    private int mNumber;
    private String mLatitude;
    private String mLongitude;
    private String mTitle;
    private ArrayList<String> mImagePath = new ArrayList<String>();
    private ArrayList<String> mNote = new ArrayList<String>();
    private ArrayList<String> mVideoPath = new ArrayList<String>();

    public SaveMarker(int number, String latitude, String longitude, String title){
        this.mNumber = number;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        this.mTitle = title;
    }

    public SaveMarker(int number, String latitude, String longitude, String title, String imagePath, String note, String videoPath) {
        this(number, latitude, longitude, title);
        this.mImagePath.add(imagePath);
        this.mNote.add(note);
        this.mVideoPath.add(videoPath);
    }

    public SaveMarker(int number, String latitude, String longitude, String title, ArrayList<String> imagePath, ArrayList<String> videoPath, ArrayList<String> note) {
        this(number, latitude, longitude, title);
        this.mImagePath = imagePath;
        this.mNote = note;
        this.mVideoPath = videoPath;
    }

    public MarkerOptions getMOptions() {
        MarkerOptions mOptions = new MarkerOptions()
                .title(mTitle)
                .position(new LatLng(Float.parseFloat(mLatitude), Float.parseFloat(mLongitude)))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        return mOptions;
    }

    public void setImagePath(String imagePath) {
        if (imagePath != null)
            mImagePath.add(imagePath);
    }

    public void delImagePath(int index) {
        if (mImagePath.size() > index)
            mImagePath.remove(index);
    }
    public void delVideoPath(int index) {
        if (mVideoPath.size() > index)
            mVideoPath.remove(index);
    }
    public void delNote(int index) {
        if (mNote.size() > index)
            mNote.remove(index);
    }

    public void setVideoPath(String videoPath) {
        if (videoPath != null)
            mVideoPath.add(videoPath);
    }

    public void setNote(String note) {
        if (note != null)
            mNote.add(note);
    }
    public void setNote(String note, int index) {
        if (note != null)
            mNote.add(index, note);
    }

    public ArrayList<String> getImagePath() {
        if (mImagePath != null)
            return mImagePath;
        return null;
    }

    public ArrayList<String> getVideoPath() {
        if (mVideoPath != null)
            return mVideoPath;
        return null;
    }

    public int getNumber() {
        return mNumber;
    }

    public ArrayList<String> getNote() {
        if (mNote != null)
            return mNote;
        return null;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mNumber);
        dest.writeString(mLatitude);
        dest.writeString(mLongitude);
        dest.writeString(mTitle);
        dest.writeList(mImagePath);
        dest.writeList(mNote);
        dest.writeList(mVideoPath);
    }

    private SaveMarker(Parcel in) {
        this.mNumber = in.readInt();
        this.mLatitude = in.readString();
        this.mLongitude = in.readString();
        this.mTitle = in.readString();
        this.mImagePath = in.readArrayList(null);
        this.mNote = in.readArrayList(null);
        this.mVideoPath = in.readArrayList(null);
    }

    public static final Parcelable.Creator<SaveMarker> CREATOR
            = new Parcelable.Creator<SaveMarker>() {
        public SaveMarker createFromParcel(Parcel in) {
            return new SaveMarker(in);
        }

        public SaveMarker[] newArray(int size) {
            return new SaveMarker[size];
        }
    };
}

class SaveMarkerManager {


    public SaveMarkerManager(Context context) {
        this.sharedPreferences = context.getApplicationContext().getSharedPreferences(SAVED_MARKERS, Context.MODE_PRIVATE);
        saveMarkerList = new ArrayList<SaveMarker>();
    }

    private ArrayList<SaveMarker> saveMarkerList;
    private SharedPreferences sharedPreferences;

    private static final String SAVED_MARKERS = "Saved_markers";

    public SaveMarker saveMarker(int markerNumber, Double lat, Double lng, String title,ArrayList<String> imagePath, ArrayList<String> videoPath, ArrayList<String> note) {
        SaveMarker newMarker = new SaveMarker(markerNumber, lat.toString(), lng.toString(), title, imagePath, videoPath, note);
        saveMarkerList.add(newMarker);
        return newMarker;
    }

    public void dropMarker(Double lat, Double lng) {
        for (int i = 0; i < saveMarkerList.size(); i++) {
            SaveMarker sMarker = saveMarkerList.get(i);
            LatLng mPosition = sMarker.getMOptions().getPosition();
            if (lat == mPosition.latitude && lng == mPosition.longitude) {
                saveMarkerList.remove(i);
                break;
            }
        }
    }

    public ArrayList<SaveMarker> getMarkerList() {
        if (saveMarkerList == null)
            return null;
        return saveMarkerList;
    }

    public void saveJSON() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        String json = new Gson().toJson(saveMarkerList);
        editor.putString(SAVED_MARKERS, json);
        editor.commit();
    }

    public void getJSON() {
        if (sharedPreferences == null)
            return;
        String jsonText = sharedPreferences.getString(SAVED_MARKERS, null);
        if (jsonText == null)
            return;
        Type type = new TypeToken<ArrayList<SaveMarker>>() {
        }.getType();
        Gson gson = new Gson();
        saveMarkerList = gson.fromJson(jsonText, type);
    }
}
