package com.example.yoga.mysecondapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Yoga on 27.02.2015.
 */
public class AddNoteActivity extends ActionBarActivity{

    String note = "";
    int position = 0;
    boolean needToChange = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        if (getIntent().getExtras() != null) {
            needToChange = true;
            note = getIntent().getStringExtra("note");
            position = getIntent().getIntExtra(SetInfoActivity.POSITION, 0);
            EditText editText = (EditText) findViewById(R.id.edit_note);
            editText.setText(note);
        }
       // ActionBar actionBar = this.getActionBar();
       // actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    public void addNoteButton(View view){
        EditText editText = (EditText) findViewById(R.id.edit_note);
        Intent intent = new Intent();
        note = editText.getText().toString();
        intent.putExtra("note", note);
        if (needToChange == true)
            intent.putExtra(SetInfoActivity.POSITION, position);
        setResult(RESULT_OK, intent);
        finish();
    }
}