package com.example.yoga.mysecondapplication;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapStateManager {

    public MapStateManager() {
        }

    public void saveMapState(GoogleMap googleMap, Bundle outState, ArrayList<MarkerOptions> markerOptList) {
        CameraPosition position = googleMap.getCameraPosition();

        outState.putFloat("latitude", (float) position.target.latitude);
        outState.putFloat("longitude", (float) position.target.longitude);
        outState.putFloat("zoom", position.zoom);
        outState.putFloat("tilt", position.tilt);
        outState.putFloat("bearing", position.bearing);
        outState.putInt("MapType", googleMap.getMapType());
        outState.putParcelableArrayList("markerOpts",markerOptList);
    }

    public CameraPosition getSavedCameraPosition(Bundle mapState) {
        double latitude = mapState.getFloat("latitude", 0);
        if (latitude == 0)
            return null;
        double longitude= mapState.getFloat("longitude", 0);
        LatLng latLng = new LatLng(latitude, longitude);
        float zoom= mapState.getFloat("zoom", 0);
        float tilt= mapState.getFloat("tilt", 0);
        float bearing= mapState.getFloat("bearing", 0);
        return new CameraPosition(latLng, zoom, tilt, bearing);
    }
    public int getMapType(Bundle mapState){
        return mapState.getInt("MapType", GoogleMap.MAP_TYPE_NORMAL);
    }

    public  ArrayList<MarkerOptions> getMarkerOpts(Bundle mapState) {
        if (mapState.containsKey("markerOpts"))
            return mapState.getParcelableArrayList("markerOpts");
        else return null;
    }

}
