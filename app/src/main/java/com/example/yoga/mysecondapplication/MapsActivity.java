package com.example.yoga.mysecondapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends ActionBarActivity implements GoogleMap.OnMapLongClickListener, LocationListener, GoogleMap.OnMarkerClickListener {

    private static final float DEFAULTZOOM = 16;
    private static final int GPS_ERRORDIALOG_REQUEST = 9001;
    private static final String PROX_ALERT_INTENT = "com.example.yoga.mysecondapplication.activity.proximity";
    MarkerInfoDialog dialogFragment;
    android.app.FragmentManager fragmentManager;
    ArrayList<SaveMarker> markerList;
    SaveMarkerManager saveMarkerManager;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    Marker mMarker;
    Location myLocation;
    LocationManager locationManager;
    String provider;
    Marker toSaveMarker; // marker which the user clicked on
    boolean isFoundLocation = false;
    ArrayList<MarkerOptions> markerOptList = new ArrayList<MarkerOptions>();
    MapStateManager mapStateManager = new MapStateManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (servicesOK()) {
            setContentView(R.layout.activity_maps);
            setUpMapIfNeeded();
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);

            myLocation = locationManager.getLastKnownLocation(provider);
            if (myLocation != null) {
                LatLng userLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, DEFAULTZOOM));
                isFoundLocation = true;
            }
            locationManager.requestLocationUpdates(provider, 60000, 1, this);
            saveMarkerManager = new SaveMarkerManager(this);
            saveMarkerManager.getJSON();
            setSavedMarkers();
        } else {
            setContentView(R.layout.dialog_marker_info);
        }
        onNewIntent(this.getIntent());
    }

    private void setSavedMarkers() {
        markerList = saveMarkerManager.getMarkerList();
        if (markerList != null) { // If locations are already saved
            for (int i = 0; i < markerList.size(); i++) { // Iterating through all the locations stored
                mMap.addMarker(markerList.get(i).getMOptions());
            }
        }
    }

    public boolean servicesOK() {
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable, this, GPS_ERRORDIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't connect to Google Play services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
        if (isFoundLocation == false) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, DEFAULTZOOM));
            isFoundLocation = true;
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapStateManager.saveMapState(mMap, outState, markerOptList);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        CameraPosition cameraPosition = mapStateManager.getSavedCameraPosition(savedInstanceState);
        mMap.setMapType(mapStateManager.getMapType(savedInstanceState));
        if (cameraPosition != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.moveCamera(cameraUpdate);
        }
        markerOptList = mapStateManager.getMarkerOpts(savedInstanceState);
        if (markerOptList != null)
            for (int i = 0; i < markerOptList.size(); i++) {
                mMap.addMarker(markerOptList.get(i));
            }
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        // locationManager.requestLocationUpdates(provider, 60000, 10, this);
    }
    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            if (mMap != null) {
                Log.e("", "Into full map");
                mMap.setMapType(mMap.MAP_TYPE_NORMAL);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                // Check if we were successful in obtaining the map.
                if (mMap != null) {
                    setUpMap();
                }
            }
        }
    }
    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }
    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMapLongClickListener(this);
        // mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
    }
    @Override
    public boolean onMarkerClick(final Marker marker) {
        boolean isSaved = false;
        toSaveMarker = marker;
        LatLng toSaveMarkerPosition = toSaveMarker.getPosition();
        dialogFragment = new MarkerInfoDialog();
        Bundle argMarker = new Bundle();
        if (markerList != null)
            for (int i = 0; i < markerList.size(); i++) {
                SaveMarker sMarker = markerList.get(i);
                LatLng sMarkerPosition = sMarker.getMOptions().getPosition();
                if (sMarkerPosition.latitude == toSaveMarkerPosition.latitude &&
                        sMarkerPosition.longitude == toSaveMarkerPosition.longitude) {
                    isSaved = true;
                    argMarker.putBoolean("isSaved",isSaved);
                    argMarker.putParcelable("isSavedMarker", sMarker);
                }
            }
        if (isSaved == false) {
            argMarker.putBoolean("isSaved",isSaved);
            argMarker.putDouble("latitude", toSaveMarkerPosition.latitude);
            argMarker.putDouble("longitude", toSaveMarkerPosition.longitude);
            argMarker.putString("title", toSaveMarker.getTitle());
        }
        dialogFragment.setArguments(argMarker);
        fragmentManager = getFragmentManager();
        dialogFragment.show(fragmentManager, "dialogFragment");
        return true;
    }
    public void onResultDialog(boolean isSaved, SaveMarker receivedMarker) {
        int mNumber = 0;
        boolean alertExists = false;
        Log.v("FIRST", "NUMBER IS " + receivedMarker.getNumber());
        LatLng savePosition = receivedMarker.getMOptions().getPosition();
        SaveMarker sMarker;
        if (markerList != null) {
            mNumber = markerList.size();
            for (int i = 0; i < markerList.size(); i++) {
                sMarker = markerList.get(i);
                LatLng sPosition = sMarker.getMOptions().getPosition();
                if (sPosition.latitude == savePosition.latitude
                        && sPosition.longitude == savePosition.longitude) {
                    saveMarkerManager.dropMarker(savePosition.latitude, savePosition.longitude);
                    mNumber = sMarker.getNumber();
                    alertExists = true;
                }
            }
        }
        if (isSaved) {
            receivedMarker = saveMarkerManager.saveMarker(mNumber, savePosition.latitude, savePosition.longitude, receivedMarker.getMOptions().getTitle(), receivedMarker.getImagePath(), receivedMarker.getVideoPath(), receivedMarker.getNote());
            markerOptList.clear();
            Log.v("SECOND", "NUMBER IS " + receivedMarker.getNumber());
            if (mMarker != null)
                mMarker.remove();
            if (alertExists == false)
                setAlert(receivedMarker);
        }
        else  if (alertExists == true)
            dropAlert(mNumber);


        saveMarkerManager.saveJSON();
        mMap.clear();
        setSavedMarkers();
        if (markerOptList != null)
            for (int i = 0; i < markerOptList.size(); i++) {
                mMap.addMarker(markerOptList.get(i));
            }
    }

    public void dropAlert(int alertNumber){
        Log.v("DROPALERT STARTS", "NUMBER IS " + alertNumber);
        Intent intent = new Intent(PROX_ALERT_INTENT + alertNumber);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        locationManager.removeProximityAlert(pendingIntent);
    }
    public void setAlert(SaveMarker receivedMarker){
        Log.v("SETALERT STARTS", "NUMBER IS " + receivedMarker.getNumber());
        IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT + receivedMarker.getNumber());
        registerReceiver(new ProximityReciever(), filter);
        Intent intent = new Intent(PROX_ALERT_INTENT + receivedMarker.getNumber());
        intent.putExtra("EventIDIntentExtraKey", receivedMarker.getNumber());
        intent.putExtra("marker",receivedMarker);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        locationManager.addProximityAlert(receivedMarker.getMOptions().getPosition().latitude, receivedMarker.getMOptions().getPosition().longitude, 2000, -1, pendingIntent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (dialogFragment == null){
            fragmentManager  = getFragmentManager();
            dialogFragment = (MarkerInfoDialog) fragmentManager.findFragmentByTag("dialogFragment");
        }
        dialogFragment.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onMapLongClick(LatLng latLng) {
        Geocoder gc = new Geocoder(MapsActivity.this);
        List<Address> list = null;
        try {
            list = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Address add = new Address(Locale.ROOT);
        if (list != null&& list.size() != 0)
            add = list.get(0);
        MapsActivity.this.setMarker(add,
                latLng.latitude, latLng.longitude);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            int id = Integer.parseInt(intent.getDataString().substring(5));
            SaveMarker sMarker = intent.getParcelableExtra("marker"+id);
            CameraUpdate cameraupdate = CameraUpdateFactory.newLatLngZoom(sMarker.getMOptions().getPosition(), DEFAULTZOOM);
            mMap.moveCamera((cameraupdate));
            this.onMarkerClick(mMap.addMarker(sMarker.getMOptions()));
        }
    }
    private String setInfoString(Address address) {
        String infoString = String.format("%s %s %s",
                address.getMaxAddressLineIndex() > 0 ?
                        address.getAddressLine(0)
                        : (address.getFeatureName() != null ?
                        address.getFeatureName() : ""),
                address.getLocality() != null ?
                        address.getLocality() : "",
                address.getCountryName() != null ?
                        address.getCountryName() : "");
        return infoString;
    }

    private void setMarker(Address address, double lat, double lng) {
        markerOptList.clear();
        mMap.clear();
        setSavedMarkers();
        if (mMarker != null)
            mMarker.remove();
        LatLng latlng = new LatLng(lat, lng);
        MarkerOptions options = new MarkerOptions()
                .title(setInfoString(address))
                .draggable(true)
                .position(latlng)
                .icon(BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_RED));
        mMarker = mMap.addMarker(options);
        markerOptList.add((options));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hybrid_type:
                mMap.setMapType(mMap.MAP_TYPE_HYBRID);
                break;
            case R.id.normal_type:
                mMap.setMapType(mMap.MAP_TYPE_NORMAL);
                break;
            case R.id.satellite_type:
                mMap.setMapType(mMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.terrain_type:
                mMap.setMapType(mMap.MAP_TYPE_TERRAIN);
                break;

        }
        return true;
    }

    public void searchPlace(View view) throws IOException {
        mMap.clear();
        setSavedMarkers();
        markerOptList.clear();
        EditText editText = (EditText) findViewById(R.id.search_et);
        hideSoftKeyboard(MapsActivity.this);
        String location = editText.getText().toString();

        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> list = geocoder.getFromLocationName(location, 10);
        if (list != null && list.size() > 0) {
            double myLong = 0;
            Address nearestAddress = (Address) list.get(0);
            if (myLocation != null)
                myLong = myLocation.getLongitude();
            double nearestLong = Double.MAX_VALUE;
            for (int i = 0; i < list.size(); i++) {

                Address address = list.get(i);
                double currentLat = address.getLatitude();
                double currentLong = address.getLongitude();

                LatLng latLng = new LatLng(currentLat, currentLong);
                if (Math.abs(currentLong - myLong) < nearestLong) {
                    nearestLong = Math.abs(currentLong - myLong);
                    nearestAddress = address;
                }
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                markerOptions.position(latLng);
                markerOptions.draggable(true);
                markerOptions.title(setInfoString(address));
                mMap.addMarker(markerOptions);
                markerOptList.add((markerOptions));
            }
            gotoLocation(nearestAddress.getLatitude(), nearestAddress.getLongitude(), DEFAULTZOOM);
        }

    }

    private void gotoLocation(double latitude, double longitude, float zoom) {
        LatLng ll = new LatLng(latitude, longitude);
        CameraUpdate cameraupdate = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mMap.moveCamera((cameraupdate));
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

}